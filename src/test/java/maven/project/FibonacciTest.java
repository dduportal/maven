package maven.project;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static maven.project.Fibonacci.fibonacciSuite;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FibonacciTest {
    @Test
    public void testFibonacciSuite() {
        assertEquals(0, fibonacciSuite(0) );
        assertEquals(1, fibonacciSuite(1) );
        assertEquals(0, fibonacciSuite(-1) );
        assertEquals(0, fibonacciSuite(Long.MIN_VALUE) );
        assertEquals(233, fibonacciSuite(13) );
    }


}