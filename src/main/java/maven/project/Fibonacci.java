package maven.project;

public class Fibonacci {
	
	public static long fibonacciSuite(long n) {
		  if (n < 0) n = 0;
	      if (n == 1 || n == 0) {
	        return n;
	      }
	      else {
	        return fibonacciSuite(n-2) + fibonacciSuite(n-1);
	      }
	  }
}
